## Flow Control

### **Introduction:**
This lesson will walk you through the control flow, which is the order your python scripts operate. Python uses the typical flow control statements. 

### **Topics Covered:**

* [**Operators**](operators.html#operators)
* [**I/O Print**](io_print.html#io-print)
* [**I/O Files**](io_files.html#io-files)
* [**If, Elif, Else**](if_elif_else.html#if-elif-else)
* [**While Loops**](while_loops.html#while-loops)
* [**For Loops**](for_loops.html#for-loops)
* [**Break and Continue**](break_continue.html#break-and-continue)


#### To access the Control Flow slides please click [here](slides)

