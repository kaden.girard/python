## **Classes** 
One key aspect of object oriented programming is the use of classes. 
For example, by creating Class Cookie, a programmer can easily create cookie objects, each with unique, but often similar characteristics. Classes are used to save the programmer time. Instead of manually specifying, from scratch, the variables associated with a cookie, the class template prepares each cookie instance to track those variables for each cookie. 

## **Design Patterns in Python**
In Python and other Object Oriented Programming there are a few approaches to overarching design of classes. These approaches are referred to as design patterns. 

Design Patterns extend the idea of how and when specific classes are used or defined. 
There are multiple categories of design patterns in Python:  creational, structural, and behavioral.
Here, we look at the singleton, adapter, and bridge design patterns. [There are more design patterns in Python - for more information see [here](https://www.geeksforgeeks.org/python-design-patterns/) .] 

### **The Singleton Design Pattern** : There can only be one 
***The singleton*** is the most straightforward design pattern. At its simplest, the singleton design pattern indicates that there can only be one instance of the class at a time. 
See also: [here](https://refactoring.guru/design-patterns/singleton) and [here](https://stackoverflow.com/questions/6760685/creating-a-singleton-in-python)
For example, given class Highlander:
```Python
class Highlander:
   highlandercount = None
   def __init__(self, name, age):
       self.name = name
       self.age = age
       if Highlander.highlandercount != None:
           raise Exception('There can only be one!')
       else:
           Highlander.highlandercount = 1
 ```

One highlander can be created:
```Python
Connor= Highlander("Connor", 36)
```


However, subsequent attempts to create a Highlander will fail. 
```Python
Ramirez= Highlander("Ramirez", 47) 
```

```
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File … line 9, in __init__
    raise Exception('There can only be one!')
Exception: There can only be one!
```

### **The Adapter Design Pattern**: How do you convert from apples to oranges?

***The adapter design pattern*** can be a class or object that allows the conversion from one type of object to another. It’s core functionality is to take a target and define what it should be adapted to. For example, with the classes Apple and Oranges the class Adapter could be used to generate an Orange object O from an Apple object A i.e. Adapter(A,O). There are class adapters and object adapters.
See also: [here](https://stackabuse.com/structural-design-patterns-in-python/#adapter),  [here](https://www.giacomodebidda.com/adapter-pattern-in-python/), and [here](http://ginstrom.com/scribbles/2009/03/27/the-adapter-pattern-in-python/) 



### **The Bridge design pattern**: Connecting classes that can be very different 
Imagine being put in charge of creating ‘aircraft.’ Within the scope of your job, you create ‘aircraft’ objects using the overarching ‘Aircraft’ class. For organizational and design purposes, this can help simplify. However, there will need to be a lot of differentiation in creating a Blackhawk helicopter and a Piper Cub airplane. 
You will create the overarching class Aircraft and it will have subclasses Airplane and Helicopter with further subclasses. However, some aspects of Helicopters, Airplanes, and any subclasses will be shared. For example, regardless of class or subclass, the objects will still have to be ordered, purchased, inventoried, etc. Rather than having those individual methods defined for each subclass - they can be defined and called from a bridge class. For example, they could all share the Administrative class that could be developed independently and would contain administrative functions, e.g. purchase, order, etc. that could be shared across classes and subclasses. 

See also: [here](https://stackabuse.com/the-bridge-design-pattern-in-python/).
