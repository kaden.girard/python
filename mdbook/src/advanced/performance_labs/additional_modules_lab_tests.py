import unittest
import os

from mdbook.src.advanced.performance_labs.additional_modules_lab_solution import find_file_with_os, \
   find_file_with_glob, find_file_with_subprocess


class StandardCases(unittest.TestCase):
    def test_standard_case(self):
        filepath = os.path.normpath(r'additional_modules_dir\deep\file8675309.txt')

        self.assertEqual(filepath, os.path.normpath(find_file_with_os()))
        self.assertEqual(filepath, os.path.normpath(find_file_with_glob()))
        self.assertEqual(filepath, os.path.normpath(find_file_with_subprocess()))


if __name__ == '__main__':
    unittest.main()
